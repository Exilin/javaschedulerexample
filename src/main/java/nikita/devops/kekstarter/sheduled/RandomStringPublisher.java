package nikita.devops.kekstarter.sheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
public class RandomStringPublisher {

    private static final Logger logger = LoggerFactory.getLogger(RandomStringPublisher.class);

    private List<String> scavPearls = new ArrayList<>();

    @Scheduled(fixedDelayString = "${schedule.delay-time}")
    public void sayThat() {
        logger.info(getPearl());
    }

    private String getPearl() {
        scavPearls.add("1");
        scavPearls.add("2");
        scavPearls.add("3");
        scavPearls.add("4");
        scavPearls.add("5");

        Random r = new Random();
        return scavPearls.get(r.nextInt(scavPearls.size()));
    }
}
